package com.flipkart.app.fllipkartbrand.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.flipkart.app.flipkartbrand.constants.AppConstantas;

@Entity
@Table(name = AppConstantas.FLIPKART_INFO)
public class Flipkart implements Serializable {

	@Id
	@Column(name = "id")
	private long id;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "established_year")
	private long establishedYear;
	
	@Column(name = "brand_value")
	private String brandValue;
	
	@Column(name = "origin_country")
	private String originCountry;
	
	public Flipkart() {}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getEstablishedYear() {
		return establishedYear;
	}

	public void setEstablishedYear(long establishedYear) {
		this.establishedYear = establishedYear;
	}

	public String getBrandValue() {
		return brandValue;
	}

	public void setBrandValue(String brandValue) {
		this.brandValue = brandValue;
	}

	public String getOriginCountry() {
		return originCountry;
	}

	public void setOriginCountry(String originCountry) {
		this.originCountry = originCountry;
	}
	
}
