package com.flipkart.app.fllipkartbrand;

import com.flipkart.app.flipkartbrand.respository.FlipkartRespository;
import com.flipkart.app.fllipkartbrand.entity.Flipkart;


public class App 
{
    public static void main( String[] args )
    {
        Flipkart flipkart = new Flipkart();
        flipkart.setId(101L);
        flipkart.setName("Peter England");
        flipkart.setEstablishedYear(1889L);
        flipkart.setBrandValue("USD 48.3 billion");
        flipkart.setOriginCountry("India");
        
        FlipkartRespository flipkartRespository = new FlipkartRespository();
        flipkartRespository.saveFlipkartDetails(flipkart);
        
    }
}
