package com.flipkart.app.flipkartbrand.respository;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.flipkart.app.fllipkartbrand.entity.Flipkart;

public class FlipkartRespository {
	
	public void saveFlipkartDetails(Flipkart flipkart) {
		try {
			
			Configuration cfg = new Configuration();
			cfg.configure();
			SessionFactory sessionFactory = cfg.buildSessionFactory();
			Session session = sessionFactory.openSession();
			Transaction transaction = session.beginTransaction();
			session.save(flipkart);
			transaction.commit();
			
		} catch (HibernateException e) {
		}
	}

}
